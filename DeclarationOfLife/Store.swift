//
//  StateHandler.swift
//  Todos
//
//  Created by Manuel Meyer on 12/04/2021.
//

typealias Access   = (                    ) -> Life
typealias Change   = ( Life.Change...     ) -> ()
typealias Reset    = (                    ) -> ()
typealias Pause    = ( Bool               ) -> ()
typealias Callback = ( @escaping () -> () ) -> ()
typealias Destroy  = (                    ) -> ()

typealias Store = (  life: Access,
             change: Change,
              reset: Reset,
              pause: Pause,
            updated: Callback,
            destroy: Destroy )

func life   (in store:Store                    ) -> Life { store.life()                    }
func change (_  store:Store,_ cs:Life.Change...)         { cs.forEach { store.change($0) } }
func reset  (_  store:Store                    )         { store.reset();                  }
func destroy(_  store:inout Store!             )         { store.destroy(); store = nil    }
