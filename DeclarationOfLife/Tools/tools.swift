//
//  tools.swift
//  DeclarationOfLife
//
//  Created by vikingosegundo on 07/03/2022.
//

func stateFor(coordinate: Life.Cell.Coordinate, in vs: ViewState) -> Life.Cell.State { vs.stateForCoordiantes[coordinate] ?? .dead }
