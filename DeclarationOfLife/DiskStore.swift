//
//  DiskStore.swift
//  Todos
//
//  Created by Manuel Meyer on 14.04.21.
//

import Foundation.NSFileManager
import Foundation

func createDiskStore(
    pathInDocs   p: String      = "life.state.json",
    fileManager fm: FileManager = .default
) -> Store
{
    var state = loadAppStateFromStore(pathInDocuments: p, fileManager: fm) { didSet { callbacks.forEach { $0() } } }
    var callbacks: [() -> ()] = []
    return (
        life    : { state },
        change  : { state     = change(state, $0)  ; persistStore(pathInDocuments:p, state:state, fileManager:fm) },
        reset   : { state     = initialLife()      ; persistStore(pathInDocuments:p, state:state, fileManager:fm) },
        pause   : { state     = state.alter($0 ? .pause : .unpause)                                               },
        updated : { callbacks = callbacks + [$0]                                                                  },
        destroy : { destroyStore(pathInDocuments:p,                                               fileManager:fm) }
    )
}

//MARK: -
private func persistStore(pathInDocuments: String, state: Life, fileManager: FileManager ) {
    do {
        let encoder = JSONEncoder()
        #if DEBUG
        encoder.outputFormatting = .prettyPrinted
        #endif
        let data = try encoder.encode(state)
        try data.write(to: fileURL(pathInDocuments: pathInDocuments, fileManager: fileManager))
    } catch { print(error) }
}

private func loadAppStateFromStore(pathInDocuments: String, fileManager: FileManager) -> Life {
    do {
        return try JSONDecoder()
            .decode(Life.self, from: try Data(contentsOf:fileURL(pathInDocuments: pathInDocuments,
                                                                         fileManager: fileManager    )))
    } catch {
        print(error)
        return initialLife()
    }
}

private func destroyStore(pathInDocuments: String, fileManager: FileManager) {
    try? fileManager.removeItem(at: try! fileURL(pathInDocuments: pathInDocuments))
}

private func fileURL(pathInDocuments: String, fileManager: FileManager = .default) throws -> URL {
    try fileManager
        .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        .appendingPathComponent(pathInDocuments)
}
fileprivate func change(_ state: Life, _ change: [ Life.Change ] ) -> Life { state.alter(change) }
