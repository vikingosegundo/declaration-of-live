//
//  ContentView.swift
//  DeclarationOfLife
//
//  Created by vikingosegundo on 05/03/2022.
//

import SwiftUI

struct ContentView: View {
    public init(viewState: ViewState) {
        self.viewState   = viewState
    }
    public var body: some View {
        ZStack {
            LifeView(cellWidth:8, cellHeight:8, backgroundColor: .black, foregroundColor: .green)
                .environmentObject(viewState)
            VStack {
            Spacer()
                HStack {
                    Spacer()
                    Button { viewState.reset()                  } label: { Text("reset")                                }
                    Button { viewState.pause(!viewState.paused) } label: { Text(viewState.paused ? "unpause" : "pause") }
                    Spacer()
                }.padding().background(Color.black.opacity(0.7))
            }
        }
    }
    @ObservedObject
    private var viewState  : ViewState
}
