//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI

final class ViewState: ObservableObject {
    @Published public var stateForCoordiantes: [Life.Cell.Coordinate:Life.Cell.State] = [:]
    @Published public var paused:Bool = false
    init(store: Store) {
        self.store = store
        store.updated {
            self.process(store)
        }
        process(store)
    }
    func process(_ store: Store) {
        DispatchQueue.main.async {
            self.stateForCoordiantes = store.life().stateForCoordiantes
            self.paused = store.life().paused
        }
    }
    func change(_ c: Life.Change) { store.change(c)                     }
    func reset ()                 { store.reset ()                      }
    func pause (_ p:Bool)         { store.change(p ? .pause : .unpause) }
    private let store:Store
}
