//
//  DeclarationOfLifeApp.swift
//  DeclarationOfLife
//
//  Created by vikingosegundo on 05/03/2022.
//

import SwiftUI
private let store    : Store     = createDiskStore()
private var viewState: ViewState = ViewState(store:store)

@main
struct DeclarationOfLifeApp: App {
    var body: some Scene {
        WindowGroup { ContentView(viewState:viewState) }
    }
}

func glider() -> [Life.Cell.Coordinate] {
    [
        .init(x: 1, y: 0),
        .init(x: 2, y: 1),
        .init(x: 0, y: 2),
        .init(x: 1, y: 2),
        .init(x: 2, y: 2)
    ]
}

func gliderGun(offset c:Life.Cell.Coordinate = .init(x: 0, y: 0)) -> [Life.Cell.Coordinate] {
    [
        .init(x:c.x   , y:c.y+4),
        .init(x:c.x+01, y:c.y+4),
        .init(x:c.x   , y:c.y+5),
        .init(x:c.x+01, y:c.y+5),
        .init(x:c.x+34, y:c.y+3),
        .init(x:c.x+35, y:c.y+3),
        .init(x:c.x+34, y:c.y+4),
        .init(x:c.x+35, y:c.y+4),
        .init(x:c.x+10, y:c.y+4),
        .init(x:c.x+10, y:c.y+5),
        .init(x:c.x+10, y:c.y+6),
        .init(x:c.x+11, y:c.y+3),
        .init(x:c.x+11, y:c.y+7),
        .init(x:c.x+12, y:c.y+2),
        .init(x:c.x+13, y:c.y+2),
        .init(x:c.x+12, y:c.y+8),
        .init(x:c.x+13, y:c.y+8),
        .init(x:c.x+14, y:c.y+5),
        .init(x:c.x+15, y:c.y+3),
        .init(x:c.x+15, y:c.y+7),
        .init(x:c.x+16, y:c.y+4),
        .init(x:c.x+16, y:c.y+5),
        .init(x:c.x+16, y:c.y+6),
        .init(x:c.x+17, y:c.y+5),
        .init(x:c.x+20, y:c.y+2),
        .init(x:c.x+21, y:c.y+2),
        .init(x:c.x+20, y:c.y+3),
        .init(x:c.x+21, y:c.y+3),
        .init(x:c.x+20, y:c.y+4),
        .init(x:c.x+21, y:c.y+4),
        .init(x:c.x+22, y:c.y+1),
        .init(x:c.x+22, y:c.y+5),
        .init(x:c.x+24, y:c.y+1),
        .init(x:c.x+24, y:c.y+5),
        .init(x:c.x+24, y:c.y  ),
        .init(x:c.x+24, y:c.y+6),
    ]
}

func initialLife() -> Life { .init(coordinates:gliderGun(offset:.init(x: 10, y: 10))) }
